<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-record-assignable-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Record;

use PhpExtended\Assignable\AssignableInterface;

/**
 * AssignableRecordInterface interface file.
 * 
 * This class is a record interface that is assignable.
 * 
 * @author Anastaszor
 */
interface AssignableRecordInterface extends AssignableInterface, RecordInterface
{
	
	// nothing to do
	
}
