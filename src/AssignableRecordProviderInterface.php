<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-record-assignable-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Record;

use Iterator;

/**
 * AssignableRecordProviderInterface interface file.
 * 
 * This interface specifies a record provider that provides assignable records.
 * 
 * @author Anastaszor
 */
interface AssignableRecordProviderInterface extends RecordProviderInterface
{
	
	/**
	 * Gets all the records for the given namespace and classname that are
	 * already assigned.
	 * 
	 * @param string $namespace
	 * @param string $classname
	 * @return Iterator<AssignableRecordInterface>
	 */
	public function getAllAssignedRecords(?string $namespace, ?string $classname) : Iterator;
	
	/**
	 * Gets all the records for the given namespace and classname that are 
	 * not assigned.
	 * 
	 * @param string $namespace
	 * @param string $classname
	 * @return Iterator<AssignableRecordInterface>
	 */
	public function getAllUnassignedRecords(?string $namespace, ?string $classname) : Iterator;
	
	/**
	 * Gets all the records that are assigned to this specific assignment id.
	 * 
	 * @param string $namespace
	 * @param string $classname
	 * @param string $assignmendId
	 * @return Iterator<AssignableRecordInterface>
	 */
	public function getAssignedRecords(string $namespace, string $classname, string $assignmendId) : Iterator;
	
}
