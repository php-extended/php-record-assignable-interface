<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-record-assignable-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Record;

use Countable;
use Iterator;
use Stringable;

/**
 * AssignableRecordProviderListInterface interface file.
 * 
 * This interface creates an iterable and countable list of AssignableRecordProviders.
 * 
 * @author Anastaszor
 * @extends \Iterator<int, AssignableRecordProviderInterface>
 */
interface AssignableRecordProviderListInterface extends Countable, Iterator, Stringable
{
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Record\RecordProviderListInterface::current()
	 */
	public function current() : AssignableRecordProviderInterface;
	
}
